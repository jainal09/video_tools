
# Installation
## Mysql
```
sudo apt-get install mysql-server
```
> Start Mysql Server
>
```
sudo systemctl start mysql
```
> Create a mysql user "general"
>
```
sudo mysql
```
```mysql
CREATE USER 'general'@'localhost' IDENTIFIED BY '1234';
GRANT ALL PRIVILEGES ON *.* TO 'general'@'localhost' IDENTIFIED BY '1234';
CREATE DATABASE multipi;
```

## Redis
```
sudo apt-get install redis-server
```
> Start Redis
>
```
redis-server
```
## Mysql Client Lib
```
sudo apt-get install libmysqlclient-dev
```
## Virtual Environment
```
sudo apt install python3-pip 
```
```
pip3 install --upgrade virtualenv
```
```
sudo apt install virtualenv
```
```
virtualenv -p python3 venv 
```
# Install Requirements
```
source venv/bin/activate
```
```
pip install -r requirements.txt
```

# Start Celery 
```
celery -A mpi_video_tools worker -l info
```

## Install Docker and Docker Compose
[Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

[Docker Compose](https://docs.docker.com/compose/install/)
# [NEW!] Start all the Containers
```
docker-compose up
```
# API Docs

## Upload a File for GIF, IMAGES, TRIM, SPLIT

1. Api Endpoint: `http://0.0.0.0:8000/video/upload/`
2. Parameters
> Method = POST
# 1. Video to Gif
>FORM DATA
```
file : "[file]"
fileID: "generated file id"
procedure: {
"procedure": ".gif",
"start": "00:00:00",
"end": "00:00:03",
"scale": "500"
}
```
>Scales can be 300, 320, 400, 480, 500, 540, 600, 800

>More Scalling will be 1200,300 & 300,1200

IMPORTANT: There is no space between 1200 and 300
# 2. Video to Images
```json
{
"procedure": "images"
}
```
# 3. Trim
```json
{
"procedure": "trim",
"start": "00:00:00",
"end": "00:00:03"
}
```
# 4. Split
> Provide count i.e no videos to be splitted
```json
{
"procedure": "split",
"count": 4,
"videos": {
    "video" : {
        "start": "00:00:00",
        "end": "00:00:03"
    },
    "video1" : {
        "start": "00:00:04",
        "end": "00:00:06"
    },
    "video2" : {
        "start": "00:00:07",
        "end": "00:00:09"
    },
    "video3" : {
        "start": "00:00:10",
        "end": "00:00:12"
    }
}
}
```
## Upload a File for Merge

1. Api Endpoint: `http://0.0.0.0:8000/merge/upload/`
2. Parameters
> Upload Videos as many as your choice and save their file ids in array
```
file: Video File
fileID : fileID
```

## Merge Videos
1. Api Endpoint: `http://0.0.0.0:8000/merge/videos/`
2. Parameters
> Generate a unique convert id and send along with the array of previously generated file ids
```
convertID: generated id
fileID: 52412,121,21,21,212,1dfd
```

## Download
1. Api Endpoint : `http://0.0.0.0:8000/download/make_download/`
>Method = POST

>FORM DATA
```json
{
"fileID": "generated file id or generated convert id"
}
```
## Redis CLI
```
redis_host='multipi.redis.cache.windows.net'
redis_db = '0'
redis_port = '6380'
redis_password = 'U0x2F3020SRUU2RLDjTe1XgB0miqY+xZSxkfKxV3vTU='
```
# Clean Up
IMP: Run only using sudo (ROOT)
```
sudo python3 delete.py 
```
> If in venv can use sudo python delete.py

IMP Run below without sudo 
```
python3 delete1.py
```
> If in venv can use python delete1.py