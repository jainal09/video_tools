from __future__ import absolute_import, unicode_literals
from celery import shared_task
import subprocess
from video_tools.services.redisStorageService import redisStore
import uuid
import shutil
import os
import logging
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
layer = get_channel_layer()

logger = logging
# celery task to convert video to images
@shared_task
def convertImages(fileid, originalFile, toConvertFilePath, parentFolder):
    print("original File", originalFile)
    print("filePath",toConvertFilePath)
    uuidString = str(uuid.uuid4())
    newImageFolder = toConvertFilePath + uuidString + "/"
    print("New Image Folder", newImageFolder)
    os.mkdir(newImageFolder)
    toConvertFileQuery= str(newImageFolder) + str(uuidString) + "%d.jpg"
    print(
    "query",
        toConvertFileQuery
    )
    subprocess.call(["ffmpeg", "-i", originalFile, toConvertFileQuery])
    toConvertZip= toConvertFilePath + "/" + uuidString
    shutil.make_archive(toConvertZip, 'zip', newImageFolder)
    zipFile = toConvertFilePath + "/" + uuidString + ".zip"
    redisStore.addToRedis(file_id=fileid, file_path=zipFile)
    shutil.rmtree(newImageFolder)
    socketDataToSend = {
        "file_id": fileid,
        "status": "Complete"
    }

    async_to_sync(layer.group_send)(
        'convert', {
            'type': 'send_toSocket',
            'message': socketDataToSend
        }
    )
    print("in send")
    try:
        shutil.rmtree(parentFolder)
    except Exception as e:
        logger.log(level=logging.DEBUG, msg=e)