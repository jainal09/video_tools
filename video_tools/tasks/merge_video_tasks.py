from __future__ import absolute_import, unicode_literals
from celery import shared_task
import subprocess
from video_tools.services.redisStorageService import redisRetrive
from video_tools.services.redisStorageService import redisStore
import uuid
from mpi_video_tools.settings import mediaPath
from mpi_video_tools.settings import BASE_DIR
import os
from pathlib import Path
import shutil
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
layer = get_channel_layer()
# celery tasks to merge videos
@shared_task
def merge(file_ids, convert_id):
    outputFileFolder = str(uuid.uuid4())
    inputFiles = []
    outputFileFolderPath = BASE_DIR +"/"+ mediaPath +"Users/"+ outputFileFolder +"/"
    os.mkdir(outputFileFolderPath)
    queryTextfile = outputFileFolderPath + str(uuid.uuid4()) + ".txt"
    file_ids = file_ids.split(',')
    print(file_ids)
    openFile = open(queryTextfile, "a+")
    for fileIds in file_ids:
        print(fileIds)
        path = redisRetrive.getFromRedis(fileIds)
        print(path)
        inputFiles.append(path)
        queryString = "file " + path
        openFile.write(queryString + "\n")
    openFile.close()
    mergedVideoPath = outputFileFolderPath + str(uuid.uuid4()) + ".mp4"
    subprocess.call(
        [
            "ffmpeg",
            "-f",
            "concat",
            "-safe",
            "0",
            "-i",
            queryTextfile,
            "-c",
            "copy",
            mergedVideoPath
        ]
    )
    redisStore.addToRedis(
        file_id=convert_id,
        file_path=mergedVideoPath
    )
    for file in inputFiles:
        fileParentFolder = Path(str(file)).parent
        fileParentFolderDir = str(fileParentFolder) + "/"
        shutil.rmtree(fileParentFolderDir)
    socketDataToSend = {
        "file_id": convert_id,
        "status": "Complete"
    }

    async_to_sync(layer.group_send)(
        'convert', {
            'type': 'send_toSocket',
            'message': socketDataToSend
        }
    )
    print("in send")
