from __future__ import absolute_import, unicode_literals
from celery import shared_task
import subprocess
from video_tools.services.redisStorageService import redisStore
import logging
import shutil
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
logger = logging
layer = get_channel_layer()
# clery task to trim video
@shared_task
def trim(start, end, fileid, originalFile, toConvertFile,parentFolder):
    subprocess.call(["ffmpeg", "-i", originalFile, "-ss", start, "-t", end, toConvertFile])
    redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)
    socketDataToSend = {
        "file_id": fileid,
        "status": "Complete"
    }

    async_to_sync(layer.group_send)(
        'convert', {
            'type': 'send_toSocket',
            'message': socketDataToSend
        }
    )
    print("in send")
    try:
        shutil.rmtree(parentFolder)
    except Exception as e:
        logger.log(level=logging.DEBUG, msg=e)