# import redis
# from mpi_video_tools.settings import \
# redis_host, \
# redis_db, \
# redis_password,\
# redis_port
# class redisStore:
#     def addToRedis(file_id, file_path):
#         redisObj = redis.StrictRedis(
#             host=redis_host,
#             port=redis_port,
#             db=redis_db,
#             password=redis_password,
#             ssl=True
#         )
#         storeStatus = redisObj.mset(
#             {
#                 file_id: file_path
#             }
#         )
#         if storeStatus == True:
#             redisStorageResp = {
#                 "msg": "Sucess"
#             }
#             return redisStorageResp
#         else:
#             redisStorageResp = {
#                 "msg": "Failed"
#             }
#             return redisStorageResp
# class redisRetrive:
#     def getFromRedis(file_id):
#         redisObj = redis.StrictRedis(
#             host=redis_host,
#             port=redis_port,
#             db=redis_db,
#             password=redis_password,
#             ssl=True
#         )
#         file_path = redisObj.get(file_id).decode('utf-8')
#         return file_path

import redis
# redis storage service used to add or query redis db
class redisStore:
    def addToRedis(file_id, file_path):
        redisObj = redis.Redis(
            host="redis",
            port=6379
        )
        storeStatus = redisObj.mset(
            {
                file_id: file_path
            }
        )
        if storeStatus == True:
            redisStorageResp = {
                "msg": "Sucess"
            }
            return redisStorageResp
        else:
            redisStorageResp = {
                "msg": "Failed"
            }
            return redisStorageResp
class redisRetrive:
    def getFromRedis(file_id):
        redisObj = redis.Redis(
            host="redis",
            port=6379
        )
        file_path = redisObj.get(file_id).decode('utf-8')
        return file_path
