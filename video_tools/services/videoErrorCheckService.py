import subprocess
import shlex
# video integrity check service used to used to check if given video is corrupted or not
class VidIntegrity:
    def check(video):
        cmd = "ffmpeg -v error -i" + " " + video + " " + "-f null"
        newCmd = shlex.split(cmd)
        output = subprocess.run(newCmd, capture_output=True)
        stringToRemove = "CompletedProcess(args=['ffmpeg', '-v', 'error'"
        outputStr = str(output).replace(stringToRemove, "")
        if "error" in outputStr or "no frame!" in outputStr:

           status ={
                "msg": "damaged"
            }
           return status
        else:
            status = {
                "msg": "safe"
            }
            return status


"""CompletedProcess(args=['ffmpeg', '-v', 'error', '-i', 'file_name.mp4', '-f', 'null'],
 returncode=1, stdout=b'', stderr=b'[h264 @ 0x55f57bb20ac0]"""

