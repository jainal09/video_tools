from rest_framework import serializers
from .models import Upload
# serializer for merge upload
class FileSerializer(serializers.ModelSerializer):
  class Meta():
    model = Upload
    fields = ('file', 'fileID', 'timestamp')