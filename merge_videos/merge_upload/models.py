from django.db import models
import uuid
import os

def get_file_path(instance, filename):
    filename = str(uuid.uuid4()) + "/" + str(uuid.uuid4()) + filename
    print(filename)
    return os.path.join('', filename)

class Upload(models.Model):
  file = models.FileField(upload_to=get_file_path,
                          null=True,
                          blank=True,)
  fileID = models.CharField(max_length=20, blank=False)
  timestamp = models.DateTimeField(auto_now_add=True)