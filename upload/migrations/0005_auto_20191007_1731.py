# Generated by Django 2.2.5 on 2019-10-07 17:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('upload', '0004_auto_20191007_0635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='upload',
            name='fileID',
            field=models.CharField(max_length=40),
        ),
    ]
