from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import re_path
from notifier.consumers import StatusConsumer
application = ProtocolTypeRouter({
    # Socket routing configuration file to route socket address to ws://host/ws/ address
    'websocket': URLRouter([
        re_path('ws/', StatusConsumer),
    ])
})
